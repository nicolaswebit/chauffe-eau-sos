<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'webit_cheausos');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZWmt%m5krQO*e%8)I]IiqYU06!pM1Sm&Wa>l/Vt[k>SN^Dc]j@]n[FiM|<(F7^^9');
define('SECURE_AUTH_KEY',  'u1ifMBfCjop!:F,wXf<a5V~C[(M$gAV~F#%$5;W{[6=}bq.fsK1Q-[U=4Bg;=U-1');
define('LOGGED_IN_KEY',    'M(nQ%IZIeIumag4;]H&=?~*h)2vKcK^jIaJ*G_v91C)h;Cw:BBQe^0rL@i,<G!9u');
define('NONCE_KEY',        ',u zd4fUOVRBrt%9O|7]4:#gD?nr1P{|SfSzl )|]=bVvlX;:mCd,}4T2P`[7j:^');
define('AUTH_SALT',        'Xuv?P]&#zmEvhV[QXq<%ts(W88p[_4TlT^w*/,/|pJ/4P||[|4@b;:S2;SOGdqQe');
define('SECURE_AUTH_SALT', 'n:DnsmSqbK3-_{%Eji<<h;me_4fBXM$n=n<{P,OPO!gun6YMO`}M~Z6 3.FKcT2G');
define('LOGGED_IN_SALT',   'xu.mxW(j9y hl$U[+`B^81lC:E=!D>[qSSLHVYXSPqwZ}9V*w/ots<1OQ^)hL/Cu');
define('NONCE_SALT',       'S=9#1p*V]#} bf@u>|9LmfXUxLrE(s-Kc15RvAiP m<?N#wokyzU+vceQdqxBhB8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
