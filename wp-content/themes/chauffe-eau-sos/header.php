<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

	<link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/bootstrap/css/bootstrap-flex.min.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/main.css">


	<script src="https://use.fontawesome.com/e2977ffe1e.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
</head>

<body>
	<div class="top-fixed">
		<div class="container">
			<div class="row flex-items-xs-middle">
				<div class="col-xs-6 ">
					<?php if (get_field('texte_cote_gauche', 'option')){
						echo "<p>" . get_field('texte_cote_gauche', 'option') . "</p>";
					} ?>
				</div>
				<div class="col-xs-6 text-right">
					English
				</div>
			</div>
		</div>
	</div>
	<div class="header fond-blanc">
		<div class="container">
			<div class="row flex-items-xs-middle">
				<div class="col-xs-5">
					<?php if (get_field("logo_header", "option")) {
						echo "<img src='" . get_field('logo_header', 'option') . "'>";
					} ?>
				</div>
				<div class="col-xs-7 text-right">
					<div class="row flex-items-xs-middle">
						<div class="col-xs-10 no-pad left">
							<?php wp_nav_menu(array('menu' => 'Main') ) ?>
						</div>
						<div class="col-xs-2">
								<div id="caller">
									<i class="fa fa-phone" aria-hidden="true"></i>
									<?php include_once("inc/caller.php") ?>
								</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('.menu-item-has-children').addClass("fleche-after");
	</script>
