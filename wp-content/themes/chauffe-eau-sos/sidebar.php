<?php ?>
<aside>
	<?php if (get_field("numero_telephone_bande","option")) { ?>
	<div class="top">
		<a href="tel:<?php echo get_field("numero_telephone_bande","option"); ?>">
			<?php echo get_field("numero_telephone_bande","option"); ?>
		</a>
	</div>
	<?php } ?>
	<?php if (get_field("image_sidebar","option")) {
		echo "<img src='" . get_field("image_sidebar","option") . "'>";
	} ?>
	<div class="bottom">
		<div class="barre"></div>
		<div class="row in-bottom flex-items-xs-middle">
			<?php if (get_field("bonhomme_sourire","option")) {
				echo "<img class='sourire' src='" . get_field("bonhomme_sourire","option") . "'>";
			} ?>
			<div class="right-in-bottom">
				<?php if (get_field("notification_top","option")) {
					echo get_field("notification_top","option");
				}
					if (get_field("notification_milieu","option")) {
						echo "<span>" . get_field("notification_milieu","option") . "</span>";
					}
					if (get_field("notification_bottom","option")) {
						echo "<p class='bot-bottom'>" . get_field("notification_bottom","option") . "</p>";
					}
				?>
			</div>
		</div>
	</div>
</aside><!-- #secondary -->
<?php ?>
