<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="inner-page container conteneur-content">
  <div class="row">
    <?php if (get_field('affichage_side_bar') == 'oui') { ?>
	     <div class="col-xs-8">
         <?php
             while ( have_posts() ) : the_post();
                  the_content();
             endwhile;
             wp_reset_query();
             ?>
	     </div>
       <div class="col-xs-4">
         <?php get_sidebar(); ?>
       </div>
     <?php } else if (get_field('affichage_side_bar') == 'non') { ?>
        <div class="col-xs-12">
          <?php
              while ( have_posts() ) : the_post();
                   the_content();
              endwhile;
              wp_reset_query();
              ?>
        </div>
     <?php } ?>
  </div>
  <div class="row">
    <div class="col-xs-12 contenuPlus">
      <?php if (get_field("image_contenu_plus")) {
        echo "<img src='" . get_field("image_contenu_plus") . "'>";
      } ?>

      <div class="conteneur-content">
        <?php if(get_field("texte_contenu_plus")){
          echo get_field("texte_contenu_plus");
        } ?>
        <?php if (get_field("bouton_contenu_plus")) {
          echo "<a class='bouton' href='" . get_field("lien_bouton_contenu_plus") . "'>" . get_field("bouton_contenu_plus") . "</a>";
        } ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer();
