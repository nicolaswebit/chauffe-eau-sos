<?php

//Caller numéro téléphone au header
 ?>
<div id="listeHeader-caller">
  <div class="container">
    <div class="row">
      <?php
      if( have_rows('liste_telephone_un','option') ):

        while ( have_rows('liste_telephone_un','option') ) : the_row();

          echo "<div class='col-xs-6 text-left'>";
            echo "<p>" . get_sub_field('ville','option') . "</p>";
          echo "</div>";

          echo "<div class='col-xs-6 text-left number'>";
            echo "<a href='" . get_sub_field('ville','option') . "'>";
              echo get_sub_field('numero','option');
            echo "</a>";
          echo "</div>";

        endwhile;

      else :

        // no rows found

      endif;

      if( have_rows('liste_telephone_deux','option') ):

        while ( have_rows('liste_telephone_deux','option') ) : the_row();

          echo "<div class='col-xs-6 text-left'>";
            echo "<p>" . get_sub_field('ville','option') . "</p>";
          echo "</div>";

          echo "<div class='col-xs-6 text-left number'>";
            echo "<a href='" . get_sub_field('ville','option') . "'>";
              echo get_sub_field('numero','option');
            echo "</a>";
          echo "</div>";

        endwhile;

      else :

        // no rows found

      endif;

      if( have_rows('liste_telephone_trois','option') ):

        while ( have_rows('liste_telephone_trois','option') ) : the_row();

          echo "<div class='col-xs-6 text-left'>";
            echo "<p>" . get_sub_field('ville','option') . "</p>";
          echo "</div>";

          echo "<div class='col-xs-6 text-left number'>";
            echo "<a href='" . get_sub_field('ville','option') . "'>";
              echo get_sub_field('numero','option');
            echo "</a>";
          echo "</div>";

        endwhile;

      else :

        // no rows found

      endif; ?>
    </div>
  </div>
</div>
<?php  ?>
