<?php get_header(); ?>
<div id="front" class="red">
  <div class="container">
    <div class="row">
      <div class="col-xs-7 gauche-front">
        <?php if (get_field("titre_front")) {
          echo "<h1 class='titre-gauche-front'>" . get_field("titre_front") . "</h1>";
        } else{
          echo "<h1>225 000 + clients satisfaits</h1>";
        }
          if (get_field("sous_titre_front")) {
            echo "<h2 class='sous-titre-gauche-front'>" . get_field("sous_titre_front") . "</h2>";
          } else{
            echo "<h2>Votre expert en chauffe-eau électrique et chauffe-au gaz</h2>";
          }
        ?>
        <div class="icons-gauche-front">
          <?php
            if (get_field("image_icon_gauche")  && get_field("titre_icon_gauche")) {
              echo "<a href='" . get_field('lien_icon_gauche') . "'>";
                echo "<img src='" . get_field('image_icon_gauche') . "'>";
                echo "<p>" . get_field('titre_icon_gauche') . "</p>";
              echo "</a>";
            }

            if (get_field("image_icon_droite")  && get_field("titre_icon_droite")) {
              echo "<a href='" . get_field('lien_icon_droite') . "'>";
                echo "<img src='" . get_field('image_icon_droite') . "'>";
                echo "<p>" . get_field('titre_icon_droite') . "</p>";
              echo "</a>";
            }
          ?>
        </div>
      </div>
      <div class="col-xs-5 droite-front">
        <?php if (get_field("image_front")){
            echo "<img class='hide' src='" . get_field('image_front') . "'>";
          }
        ?>
      </div>
    </div>
  </div>
</div>

<div class="container conteneur-content">
  <div class="row">
    <?php if (get_field('affichage_side_bar') == 'oui') { ?>
	     <div class="col-xs-8">
         <?php
             while ( have_posts() ) : the_post();
                  the_content();
             endwhile;
             wp_reset_query();
             ?>
	     </div>
       <div class="col-xs-4">
         <?php get_sidebar(); ?>
       </div>
     <?php } else if (get_field('affichage_side_bar') == 'non') { ?>
        <div class="col-xs-12">
          <?php
              while ( have_posts() ) : the_post();
                   the_content();
              endwhile;
              wp_reset_query();
              ?>
        </div>
     <?php } ?>
  </div>
  <div class="row">
    <div class="col-xs-12 contenuPlus">
      <?php if (get_field("image_contenu_plus")) {
        echo "<img src='" . get_field("image_contenu_plus") . "'>";
      } ?>

      <div class="conteneur-content">
        <?php if(get_field("texte_contenu_plus")){
          echo get_field("texte_contenu_plus");
        } ?>
        <?php if (get_field("bouton_contenu_plus")) {
          echo "<a class='bouton' href='" . get_field("lien_bouton_contenu_plus") . "'>" . get_field("bouton_contenu_plus") . "</a>";
        } ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".droite-front img").removeClass("hide");
  });
</script>

<?php get_footer(); ?>
