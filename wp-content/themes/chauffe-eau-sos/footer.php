<div class="conteneurListePhone">
  <div class="container">
        <?php if (get_field('titre_haut_footer','option')) { ?>
          <div class="row">
            <div class="col-xs-12">
              <h3>
                <?php echo get_field('titre_haut_footer','option'); ?>
              </h3>
            </div>
          </div>
        <?php } ?>
    <div class="row flex-items-xs-middle">
      <div class="col-xs-12 col-sm-4">
        <div class="row">
          <?php
          if( have_rows('liste_telephone_un','option') ):

            while ( have_rows('liste_telephone_un','option') ) : the_row();

              echo "<div class='col-xs-7'>";
                echo "<p>" . get_sub_field('ville','option') . "</p>";
              echo "</div>";

              echo "<div class='col-xs-5'>";
                echo "<a href='" . get_sub_field('ville','option') . "'>";
                  echo get_sub_field('numero','option');
                echo "</a>";
              echo "</div>";

            endwhile;

          else :

            // no rows found

          endif;
          ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 middle">
        <div class="row">
          <?php
          if( have_rows('liste_telephone_deux','option') ):

            while ( have_rows('liste_telephone_deux','option') ) : the_row();

              echo "<div class='col-xs-7 padding-more'>";
                echo "<p>" . get_sub_field('ville','option') . "</p>";
              echo "</div>";

              echo "<div class='col-xs-5'>";
                echo "<a href='" . get_sub_field('ville','option') . "'>";
                  echo get_sub_field('numero','option');
                echo "</a>";
              echo "</div>";

            endwhile;

          else :

            // no rows found

          endif;
          ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="row">
          <?php
          if( have_rows('liste_telephone_trois','option') ):

            while ( have_rows('liste_telephone_trois','option') ) : the_row();

              echo "<div class='col-xs-7 padding-more'>";
                echo "<p>" . get_sub_field('ville','option') . "</p>";
              echo "</div>";

              echo "<div class='col-xs-5'>";
                echo "<a href='" . get_sub_field('ville','option') . "'>";
                  echo get_sub_field('numero','option');
                echo "</a>";
              echo "</div>";

            endwhile;

          else :

            // no rows found

          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-6 col-sm-4">
        <div class="colonne bold">
          <?php
          $menu_footer_un = get_field("menu_id_colonne_un","option");
          wp_nav_menu(array("menu" => $menu_footer_un));

          if (get_field("image_colonne_un","option")) {
            echo "<img src='" . get_field("image_colonne_un","option") . "'>";
          }
          ?>
        </div>
      </div>
      <div class="col-xs-6 col-sm-4">
        <div class="colonne padding-more">
          <?php
          $menu_footer_deux = get_field("menu_id_colonne_deux","option");
          wp_nav_menu(array("menu" => $menu_footer_deux));

          if (get_field("image_colonne_deux","option")) {
            echo "<img src='" . get_field("image_colonne_deux","option") . "'>";
          }
          ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="colonne padding-more">
          <?php
          $form_shortcode = get_field('formulaire_footer','option');
          echo do_shortcode($form_shortcode); ?>
        </div>
      </div>
    </div>
  </div>
</footer>
<div id="credit">
  <div class="container">
    <div class="row flex-items-sm-middle">
      <div class="col-xs-12 col-sm-6">
        <?php if (get_field('gauche_bas_footer','option')) {
          echo get_field('gauche_bas_footer','option');
        } ?>
      </div>
      <div class="col-xs-12 col-sm-6 text-right">
        <?php if (get_field('droite_bas_footer','option')) {
          echo get_field('droite_bas_footer','option');
        } ?>
      </div>
    </div>
  </div>
</div>


<?php wp_footer(); ?>

</body>
</html>
